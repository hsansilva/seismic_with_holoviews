"""
This module implements useful function to deal with gathers, velocity model and wiggle plots in an interactive, web-based way.
These function relies on Holoviews, which is responsible for interacting with the Bokeh's objects (javascript objects) underneath.
Plotting using Holoviews allows one to unveal the power of JS without having to touch it!
"""

import numpy as np
import holoviews as hv 
from holoviews import opts
from holoviews import streams
from holoviews.operation.datashader import regrid
hv.extension('bokeh')

def getPlanePlot(plane, cmap, origin_file_type):
    """
    This function aims to plot a plane image from a 3D numpy array. 
    It can manage to plot SEP or SEGY gathers as well as velocity models and other similar data.
    The image obtained in this way is interactive and can be zoomed in/out at will within the browser. 
    The regrid operation implements the dynamic interpolation for different zoom levels. Although this is very handy,
    it can become heavier if low-memory or big images are being treated. 
    The path plot allows one to draw shapes directly on the plot.    
    Making freehand global seems to still not be enough to be able to save the trace from clicking the save buttom. 
    Some work around needs to be found.
    
    :param plane: a 3D numpy.array obtained by one of the getters in getters.py
    :type plane: numpy.array, mandatory
    :param cmap: the matplotlib cmap color scheme
    :type cmap: str, mandatory
    :param origin_file_type: whether it is 'sep_gathers', 'segy_gathers' or 'velocity_model' like file
    :type origin_file_type: str, mandatory
    :return: a holoviews image plot 
    :rtype: holoviews dynamic plot
    """

    if origin_file_type == 'sep_gathers' or origin_file_type == 'segy_gathers':
        clip = 99
        half_range = (plane.max() - plane.min())/2
        m_clip = (half_range / 100.) * clip
        vmin = -half_range + m_clip
        vmax = -vmin        
        height=500
        width=500
    
    elif origin_file_type == 'velocity_model':
        clip=np.percentile(plane, 99.9)
        vmin = clip
        vmax = -vmin
        height=500
        width=750
    
    global freehand
    
    plane = plane[::-1,:]
    bounds=(0, 0, plane.shape[1], plane.shape[0])
    plot_opts = {
                'height': height, 
                'width': width, 
                'colorbar': True, 
                'clim':(vmin, vmax), 
                'xlabel': '',
                'ylabel': '',
                'xlim': (0, plane.shape[1]),
                'ylim': (0, plane.shape[0]),
                'xaxis': 'top',
                'invert_yaxis': True,
                }
    style_opts = {'cmap': cmap}
    path = hv.Path([])
    freehand = streams.FreehandDraw(source=path, num_objects=10, styles={'line_color': ['yellow']})
    path.opts(opts.Path(active_tools=['freehand_draw'], height=400, line_width=10, width=400))
    img = hv.Image(plane, bounds=bounds).opts(plot=plot_opts, style=style_opts)
    return regrid(img, upsample=True, interpolation='bilinear', precompute=True) * path

def PlotTrace(plane, line_id, wiggle, wiggle_limits):
    """
    This function aims to plot the wiggle plot of a given trace within the gathers file. 
    Providing names for each area and curve plot avoids mixing data ranges among different plots and different planes. 
    See my post on HoloViews' discourse: 
    https://discourse.holoviz.org/t/distinct-holoviews-plots-sharing-the-same-data-range-when-embedded-in-panel-dashboards/242
    It interpolates the trace with 20 times more points so that the area under the curve is smooth and fill it perfectly.
    If this is not done, they do not match exactly.
    Instead the plane plotter function that is called once when loading the files and all plots are stocked in a 
    namedtuple data container, this function is called every time the user interacts with the web interface around 
    this plot (choice of interval, wiggle mode, plane id, trace, etc). 
    This is only possible since the number of combinations that would need to be otherwise predicted, 
    plotted and stocked would consume much more resources than plotting the traces on-the-fly. 
    This is obviously not the case for the plots of planes. 
    
    :param plane: the plane from which the wiggle will be plotted
    :type plane: numpy.array, mandatory
    :param line_id: which line of the plane will be plotted
    :type line_id: int, mandatory
    :param wiggle: whether it is 'Positive', 'Negative' or 'None'
    :type wiggle: str, mandatory
    :param wiggle_limits: the window where the wiggle will be plotted
    :type wiggle_limits: two-integer tuple, mandatory
    :return: a holoviews plot of the wiggle plotted on-the-fly
    :rtype: holoviews dynamic plot
    """
    
    aspect=500
    
    vmin = round(wiggle_limits[0])
    vmax = round(wiggle_limits[1])
    trace = np.round(plane[line_id-1][vmin:vmax], 5)
    x = np.arange(vmin, vmax)
    x_interp = np.linspace(vmin, vmax, 20*len(x))
    trace_interp = np.interp(x_interp, x, trace)       
    lim = 1.05*max((abs(trace.min()), trace.max()))
    
    if wiggle == 'Positive':
        w = np.array([i if i > 0 else 0 for i in trace_interp])
    elif wiggle == 'Negative':
        w = np.array([i if i < 0 else 0 for i in trace_interp])
    else:
        w = np.zeros(len(trace_interp))
        
    plot_opts_trace = {'height': aspect, 
                       'width': aspect, 
                       'xlabel': '',
                       'ylabel': '',
                       'xlim': (vmin, vmax),
                       'ylim': (-lim, lim),
                       'xaxis': 'top',
                       'invert_yaxis': True,
                       'invert_axes': True, 
                     }

    style_opts_curve = {'color': 'k'}
    style_opts_area = {'fill_color': 'k'}
    
    return hv.Area((x_interp, w), 'trace_offline_{}'.format(round(trace[-1])), 'trace_intensity_{}'.format(round(trace[-1]))).opts(plot=plot_opts_trace, style=style_opts_area) * hv.Curve((x_interp, trace_interp), 'trace_offline_{}'.format(round(trace[-1])), 'trace_intensity_{}'.format(round(trace[-1]))).opts(plot=plot_opts_trace, style=style_opts_curve)

def LineTrace(line_id, wiggle_limits):
    """
    This function traces a vertical blue line on the plane plot indicating the ensemble of data that is 
    being plotted in the wiggle plot. 
    
    :param line_id: which line of the plane will be plotted
    :type line_id: int, mandatory
    :param wiggle_limits: the window where the wiggle will be plotted
    :type wiggle_limits: two-integer tuple, mandatory
    :return: a holoviews plot of a single vertical line
    :rtype: a holoviews dynamic plot
    """
    
    vmin = round(wiggle_limits[0])
    vmax = round(wiggle_limits[1])
    
    ys = np.arange(vmin, vmax)
    xs = line_id * np.ones(len(ys))

    return hv.Curve((xs, ys)) 
