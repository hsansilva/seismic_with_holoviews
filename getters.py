"""
Definition of planes and plots getters.
These functions aim to get planes and plots from containers.
By now, they are taylored to work with namedtuples.
"""

import numpy as np

def GetPlanesAttr_sep(planes_container, file_id, plane_id, attr):
    """
    This wrapper function retrieves a plane among all the planes for a given set of filters. 
    This function is taylored for SEP files.
    
    :param planes_container: the container object with all the planes
    :type planes_container: a namedtuple object, mandatory
    :param file_id: whether it is the first or second file
    :type file_id: int, mandatory
    :param plane_id: which plane is being analysed within the SEP file
    :type plane_id: int, mandatory
    :param attr: whether one wants to get the plane data array or its maximum
    :type attr: str, mandatory
    :return: a unique plane numpy array if attr == 'plane', a float elif attr == 'plane_max', else throws a KeyError
    :rtype: numpy.array or an error
    """
    if attr == 'plane':
        return [el.plane for el in planes_container if el.file_id == file_id and el.plane_id == plane_id][0]
    elif attr == 'plane_max':
        return [el.plane_max for el in planes_container if el.file_id == file_id and el.plane_id == plane_id][0]
    else:
        raise KeyError('Parsed attribute is not known.')
        
def GetPlotsAttr_sep(plots_container, file_id, plane_id, cmap):
    """
    This wrapper function retrieves a plot of a plane among all the plots for a given set of filters. 
    This function is taylored for SEP files.
    The aim of this function is to pre-compute all the plots and stock them so that they are not plotted on-the-fly.
    
    :param plots_container: the container object with all the planes
    :type plots_container: a namedtuple object, mandatory
    :param file_id: whether it is the first or second file
    :type file_id: int, mandatory
    :param plane_id: which plane is being analysed within the SEP file
    :type plane_id: int, mandatory
    :param cmap: the cmap to be used
    :type cmap: str, mandatory, it can be gray, seismic or RdGy
    :return: a unique holoviews plot
    :rtype: a holoviews plot
    """    
    return [el.plot for el in plots_container if el.file_id == file_id and el.plane_id == plane_id and el.cmap == cmap][0]

def GetPlanesAttr_segy(planes_container, file_id):
    """
    This wrapper function retrieves a plane among all the planes for a given set of filters. 
    This function is taylored for SEGY files.
    
    :param planes_container: the container object with all the planes
    :type planes_container: a namedtuple object, mandatory
    :param file_id: whether it is the first or second file
    :type file_id: int, mandatory
    :return: a unique plane numpy array
    :rtype: numpy.array or an error
    """    
    return [el.plane for el in planes_container if el.file_id == file_id][0]
        
def GetPlotsAttr_segy(plots_container, file_id, cmap):
    """
    This wrapper function retrieves a plot of a plane among all the plots for a given set of filters. 
    This function is taylored for SEGY files.
    The aim of this function is to pre-compute all the plots and stock them so that they are not plotted on-the-fly.
    
    :param plots_container: the container object with all the planes
    :type plots_container: a namedtuple object, mandatory
    :param file_id: whether it is the first or second file
    :type file_id: int, mandatory
    :param cmap: the cmap to be used
    :type cmap: str, mandatory, it can be gray, seismic or RdGy
    :return: a unique holoviews plot
    :rtype: a holoviews plot
    """       
    return [el.plot for el in plots_container if el.file_id == file_id and el.cmap == cmap][0]
