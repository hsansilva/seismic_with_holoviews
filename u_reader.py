"""
It relies on u_sep and u_segy to read SEP and SEGY files and return appropriate data structures to be plotted.
As 24/02/2020, it requests the use of a local copy of PQCC as defined by the block below. This is the necessary since the production version of PQCC is on Python 2 and this application is written in Python 3. In this way, when we call the u_sep module, its dependance u_ascii tries to make an import that is no longer authorized in Python 3 (from __builtins__ import False). 
Once PQCC will migrate to Python 3 or at least become Python 3-compatible, the block below can be deleted and u_sep and u_segy can be found by loading environment modules. 
"""

import numpy as np
import pandas as pd

###################################################################################
### Ceci est necessaire pour eviter les incompatibilites entre PQCC et Python 3, 
### notamment dans le module u_ascii.py de la version de PROD actuelle (24/02/2020)
import sys
src_root = './src/'
sys.path.append(src_root+'lib')
sys.path.append(src_root+'utils')
####################################################################################

import u_sep
import u_segy

def getHeaderInfo_sep(file_input):
    """
    This functions aims to get the header info of a SEP file. 
    As it needs a path to the header, it defines the way the application requires the user to input the file path 
    (by pasting it within a box). 
    If the behavior of selecting a file from the file browser becomes needed, the PQCC functions 
    needed to be modified to accept the header content to be parsed instead of its path. 
    For more details, refer to my question posted on Panel's discourse page: 
    https://discourse.holoviz.org/t/panels-fileinput-widget/70
    
    :param file_input: the full path to the header file, file extension (.H) included
    :type file_input: str, mandatory
    :return: a pandas dataframe containing the header information
    :rtype: pandas.DataFrame object    
    """
    sep_parameters_in = {}
    u_sep.getHeaderInfo(sep_parameters_in, file_input);
    keys = [
        'sep_binary_in', 'd1', 'maxDim', 'numberOfSamples', 'dims', 'labels', 'mins', 
        'steps', 'maxs', 'sample_size', 'format', 'dtype', 'plane_size', 'plane_number'
    ]
    values = [
        u_sep.getBinary(file_input),
        sep_parameters_in['d1'], 
        u_sep.maxDim(sep_parameters_in), 
        u_sep.getNumberSamples(sep_parameters_in), 
        u_sep.dims(sep_parameters_in), 
        u_sep.labels(sep_parameters_in), 
        u_sep.mins(sep_parameters_in), 
        u_sep.steps(sep_parameters_in), 
        u_sep.maxs(sep_parameters_in), 
        u_sep.getSampleFormat(sep_parameters_in)[0], 
        u_sep.getSampleFormat(sep_parameters_in)[1], 
        u_sep.getSampleFormat(sep_parameters_in)[2], 
        u_sep.get2DPlaneSize(sep_parameters_in)[0], 
        u_sep.get2DPlaneSize(sep_parameters_in)[1]
    ]
    return pd.DataFrame(index=keys, data=values)

def getHeaderInfo_segy(file_input):
    """
    This functions aims to get the header info of a SEGY file. 
    If the information displayed by the application is not the relevant one, 
    it can be taylored to display more useful details of the file.

    :param file_input: the full path to the header file, file extension (.segy) included
    :type file_input: str, mandatory
    :return: a pandas series containing the header information
    :rtype: pandas.Series object    
    """        
    text = [t.strip('b\'') for t in str(u_segy.headerEbcdicPrint(file_input)).split('\n') if t != '']
    return pd.Series(data=text).to_frame()

def getPlanes_sep(file_input):
    """
    This function aims to read all planes out of a SEP file.
    
    :param file_input: the full path to the header file, file extension (.H) included
    :type file_input: str, mandatory
    :return: planes and their associate metadata
    :rtype: a tuple containing a list of numpy.arrays and a list of dictionaries
    """    
    planes = []
    metadata = []
    for i, j in u_sep.readPlanes(file_input):
        plane = np.array(i, dtype=np.float).T
        metadatum = {'plane_id': j, 'plane_shape': plane.shape, 'plane_max_amp': plane.max()}
        planes.append(plane)
        metadata.append(metadatum)
    return planes, metadata

def getPlane_segy(file_input, noftraces=100):
    """
    This function aims to read all planes out of a SEGY file.
    In the future, one needs to think how to request the number of traces parameter from the user 
    directly within the application. Maybe introduce a check to verify if the file is a segy file, 
    if yes, read the number of traces and request the user to insert how many traces they want to plot. 
    Plotting all the traces may not be an interesting strategy since it might generate huge plots 
    on the limit of memory management by the browser. 
    
    :param file_input: the full path to the header file, file extension (.segy) included
    :type file_input: str, mandatory
    :param noftraces: the number of traces to be read in the SEGY file, defaults to 100
    :rtype noftraces: int, optional
    :return: the plane read from the segy file
    :rtype: 3D numpy.array
    """       
    
    noftracesavail = u_segy.traceCount(file_input)
    if noftraces <= noftracesavail:
        pass
    else:
        raise AttributeError('Number of traces requested is higher than the number of traces available.')
    plane = []
    for i in range(noftraces):
        bytes_, _ = u_segy.sample(file_input, n_trace=i+1)
        trace = u_segy.sampleDecode(bytes_, big_endian=True)
        plane.append(trace)
    plane = np.array(plane).T
    return plane
